/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1999-2010
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "disps_fifo.h"
#include "file_source_float.h"
#include "file_sink_float.h"
#include "fir.h"
#include "util.h"
#include "wav_file_source.h"
#include "wav_file_sink.h"
#include "actor_context_type_common.h"
#include "actor_context_type_common.h"


/* An enumeration of the actors in this application. */
#define ACTOR_INSOURCE 0
#define ACTOR_FIR 1
#define ACTOR_SINK 2

/* The total number of actors in the application. */
#define ACTOR_COUNT 3
float * read_coefficients(char *, int *);
/* 
    Usage: driver.exe x_file y_file out_file
*/
int main(int argc, char **argv) {
    FILE *in_file = NULL;
    FILE *out_file = NULL;
    actor_context_type *actors[ACTOR_COUNT];
    file_source_float_context_type *in_source = NULL;
	int filter_length;
	float* filter_coeffs;
	int interpolation;
	int decimation;
    
    /* Connectivity: fifo1: (in, fir), fifo2: (fir,sink); 
    */
    disps_fifo_pointer fifo1 = NULL, fifo2 = NULL;

    int token_size = 0;
    int i,j = 0;
    int arg_count = 5;
    
    /* actor descriptors (for diagnostic output) */
    char *descriptors[ACTOR_COUNT] = {"insource", "fir", "sink"};

    /* Check program usage. */
    if (argc != arg_count) {
        fprintf(stderr, "driver.exe error: arg count");
        exit(1);
		
    }   

    /* Open the input and output file(s). */
    i = 1;
    in_file = util_fopen(argv[i++], "r"); 
    out_file = util_fopen(argv[i++], "w"); 
	interpolation = atoi(argv[i++]);
	decimation = atoi(argv[i++]);
	

    /* Create the buffers. */
    token_size = sizeof(int);
    fifo1 = disps_fifo_new(decimation, token_size);
    fifo2 = disps_fifo_new(interpolation, token_size);

    /* Create and connect the actors. */
    i = 0;
    actors[ACTOR_INSOURCE] = 
            (actor_context_type *)(file_source_float_new(in_file, fifo1));
	filter_coeffs = read_coefficients("coefs_for_fir_tests.txt", &filter_length);
    actors[ACTOR_FIR] = 
            (actor_context_type *)(fir_new(fifo1, fifo2, filter_length, filter_coeffs, interpolation, decimation, 0,1));
    actors[ACTOR_SINK] = (actor_context_type *)(file_sink_float_new(out_file, fifo2,"",0));

    in_source = (file_source_float_context_type *)(actors[ACTOR_INSOURCE]);


    /* A simple scheduler: keep executing actors until one of the
       sources has finished, and there are no more result tokens
       waiting to be written to the output file. 
    */
    while ((in_source->active)|| (disps_fifo_population(fifo2) > 0)) {
        for (i = 0; i < decimation; i++) {
            util_execute(actors[ACTOR_INSOURCE],descriptors[ACTOR_INSOURCE]);
        }
		util_execute(actors[ACTOR_FIR],descriptors[ACTOR_FIR]);
		for (j = 0 ; j < interpolation; j++){
			util_execute(actors[ACTOR_SINK],descriptors[ACTOR_SINK]);
		}
    }

    /* Normal termination. */
    return 0;
}

float* read_coefficients(char* filename, int* plen){
	FILE* fcoef = fopen(filename,"r");
	int j = 0;
	float* coef = (float*)malloc(500*sizeof(float));
	if (fcoef==NULL)
		printf("Could not open file\n");
	else
	{
		while(-1 != fscanf(fcoef,"%f",coef+(j))){
			j++;
		}
		*plen = j;
	}
	return coef;
}
