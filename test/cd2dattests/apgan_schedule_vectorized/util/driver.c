/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1999-2010
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "disps_fifo.h"
#include "file_source_float.h"
#include "file_sink_float.h"
#include "fir.h"
#include "util.h"
#include "wav_file_source.h"
#include "wav_file_sink.h"
#include "actor_context_type_common.h"
#include "actor.h"


#define BUFFER_CAPACITY_1 1 /*has to be calculated*/
#define BUFFER_CAPACITY_2 6 /*has to be calculated*/
#define BUFFER_CAPACITY_3 392 /*has to be calculated*/
#define BUFFER_CAPACITY_4 35 /*has to be calculated*/
#define BUFFER_CAPACITY_5 4 /*has to be calculated*/

/* An enumeration of the actors in this application. */
#define ACTOR_SOURCE 0
#define ACTOR_FIR1 1
#define ACTOR_FIR2 2
#define ACTOR_FIR3 3
#define ACTOR_FIR4 4
#define ACTOR_SINK 5

/* The total number of actors in the application. */
#define ACTOR_COUNT 6

float * read_coefficients(char *, int *);


/* 
    Usage: driver.exe x_file y_file out_file
*/
int main(int argc, char **argv) {
    FILE *in_file = NULL;
    FILE *out_file = NULL;
    actor_context_type *actors[ACTOR_COUNT];
    wav_file_source_context_type *in_source = NULL;
	wav_file_sink_context_type *out_sink = NULL;
	int filter_length;
	float* filter_coeffs;

    
    /* Connectivity: fifo1: (in, fir1), fifo2: (fir1, fir2); 
       fifo3: (fir2, fir3); fifo4: (fir3, fir4); fifo5: (fir4, sink)
    */
    disps_fifo_pointer fifo1 = NULL, fifo2 = NULL, fifo3 = NULL, fifo4 = NULL, fifo5 = NULL;

    int token_size = 0;
    int i, j, k = 0;
    int arg_count = 3;
    
    /* actor descriptors (for diagnostic output) */
    char *descriptors[ACTOR_COUNT] = {"insource", "fir1", "fir2", 
            "fir3", "fir4", "sink"};

    /* Check program usage. */
    if (argc != arg_count) {
        fprintf(stderr, "driver.exe error: arg count");
        exit(1);
    }   

    /* Open the input and output file(s). */
    i = 1;
    in_file = util_fopen(argv[i++], "r"); 
    out_file = util_fopen(argv[i++], "w"); 

    /* Create the buffers. */
    token_size = sizeof(float);
    fifo1 = disps_fifo_new(BUFFER_CAPACITY_1, token_size);
    fifo2 = disps_fifo_new(BUFFER_CAPACITY_2, token_size);
    fifo3 = disps_fifo_new(BUFFER_CAPACITY_3, token_size);
    fifo4 = disps_fifo_new(BUFFER_CAPACITY_4, token_size);
	fifo5 = disps_fifo_new(BUFFER_CAPACITY_5, token_size);

    /* Create and connect the actors. */
    i = 0;
    actors[ACTOR_SOURCE] = 
            (actor_context_type *)(wav_file_source_new(in_file, fifo1, 1));
	filter_coeffs = read_coefficients("coefs1.txt", &filter_length);
    actors[ACTOR_FIR1] = 
            (actor_context_type *)(fir_new(fifo1, fifo2, filter_length, filter_coeffs, 2, 1, 0, 1));
	filter_coeffs = read_coefficients("coefs2.txt", &filter_length);
    actors[ACTOR_FIR2] = 
            (actor_context_type *)(fir_new(fifo2, fifo3, filter_length, filter_coeffs, 4, 3, 0, 2));
	filter_coeffs = read_coefficients("coefs3.txt", &filter_length);
    actors[ACTOR_FIR3] = 
            (actor_context_type *)(fir_new(fifo3, fifo4, filter_length, filter_coeffs, 5, 7, 0, 7));
	filter_coeffs = read_coefficients("coefs2.txt", &filter_length);		
	actors[ACTOR_FIR4] = 
            (actor_context_type *)(fir_new(fifo4, fifo5, filter_length, filter_coeffs, 4, 7, 0, 1 ));
			
    actors[ACTOR_SINK] = (actor_context_type *)(wav_file_sink_new(out_file, fifo5, 4));

    in_source = (wav_file_source_context_type *)(actors[ACTOR_SOURCE]);
	out_sink = (wav_file_sink_context_type *)(actors[ACTOR_SINK]);
   

   
    /* A simple scheduler: keep executing actors until one of the
       sources has finished, and there are no more result tokens
       waiting to be written to the output file. 
    */
    while ((in_source->active) || (disps_fifo_population(fifo5) > 0)) {
		/*for (i = 0; i < ACTOR_COUNT; i++) {
			for (j =0; j<=count[i];j++){
				util_execute(actors[i], descriptors[i]);
			}
		}*/
		for (i = 0 ; i <49; i++){
			for (j=0; j<3; j++){
				util_execute_vectorized(actors[ACTOR_SOURCE], descriptors[0]);
				util_execute_vectorized(actors[ACTOR_FIR1], descriptors[1]);
			}
			util_execute_vectorized(actors[ACTOR_FIR2], descriptors[2]);
		}
		
		/* E(4F) = EFFFF
		//5E(4F) = EFFFF EFFFF EFFFF EFFFF EFFFF 
		// 7D 5E4F = DDDDDDD EFFFF EFFFF EFFFF EFFFF EFFFF 
		// 8 (7D) (5E(4F)) =  DDDDDDD EFFFF EFFFF EFFFF EFFFF EFFFF    DDDDDDD EFFFF EFFFF EFFFF EFFFF EFFFF    ... 8 times
		*/
		for (i=0;i<8;i++){
				util_execute_vectorized(actors[ACTOR_FIR3], descriptors[3]);
			for(k=0;k<5;k++){
				util_execute_vectorized(actors[ACTOR_FIR4], descriptors[4]);
				util_execute_vectorized(actors[ACTOR_SINK], descriptors[5]);
			}
		}
		
	}
    /* Normal termination. */
	wav_file_source_terminate(in_source);
	wav_file_sink_terminate(out_sink);
	disps_fifo_free(fifo1);
	disps_fifo_free(fifo2);
	disps_fifo_free(fifo3);
	disps_fifo_free(fifo4);
	disps_fifo_free(fifo5);
    return 0;
}

float* read_coefficients(char* filename, int* plen){
	FILE* fcoef = fopen(filename,"r");
	int j = 0;
	float* coef = (float*)malloc(500*sizeof(float));
	if (fcoef==NULL)
		printf("Could not open file\n");
	else
	{
		while(-1 != fscanf(fcoef,"%f",coef+(j))){
			j++;
		}
		*plen = j;
	}
	return coef;
}
