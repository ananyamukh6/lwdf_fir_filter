/*******************************************************************************
@ddblock_begin copyright

Copyright (c) 1999-2010
Maryland DSPCAD Research Group, The University of Maryland at College Park 

Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE UNIVERSITY OF MARYLAND BE LIABLE TO ANY PARTY
FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
THE UNIVERSITY OF MARYLAND HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.

THE UNIVERSITY OF MARYLAND SPECIFICALLY DISCLAIMS ANY WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE
PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE UNIVERSITY OF
MARYLAND HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,
ENHANCEMENTS, OR MODIFICATIONS.

@ddblock_end copyright
*******************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include "disps_fifo.h"
#include "file_source_float.h"
#include "file_sink_float.h"
#include "fir.h"
#include "util.h"
#include "wav_file_source.h"
#include "wav_file_sink.h"
#include "actor_context_type_common.h"
#include "actor_context_type_common.h"


#define BUFFER_CAPACITY_1 147
#define BUFFER_CAPACITY_2 294
#define BUFFER_CAPACITY_3 392
#define BUFFER_CAPACITY_4 280
#define BUFFER_CAPACITY_5 160

/* An enumeration of the actors in this application. */
#define ACTOR_SOURCE 0
#define ACTOR_FIR1 1
#define ACTOR_FIR2 2
#define ACTOR_FIR3 3
#define ACTOR_FIR4 4
#define ACTOR_SINK 5

/* The total number of actors in the application. */
#define ACTOR_COUNT 6

float * read_coefficients(char *, int *);


/* 
    Usage: driver.exe x_file y_file out_file
*/
int main(int argc, char **argv) {
    printf("ch1");
	return 0;
}